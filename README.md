## KC Test

---
## Requirements
  * PHP 7
  * MySQL

## Clone repository

```
git clone git@bitbucket.org:TanyaMarcol/testkc.git
```
---

## Database

Create a database called kc_db and import the file "kc_db.sql" into the database.

Replace your configuration values from "config-example.php" with your database configuration and rename the file to "config.php"

```
$settings = [
  'DB_HOST' => '',
  'DB_PORT' => 3306,
  'DB_USERNAME' => '',
  'DB_PASSWORD' => '',
  'DB_NAME' => ''
];

```
---

## Running the project

Using your termimnal, navigate to your project directory and run the following command:

```
php -S localhost:8080 -t ./
```

Paste the url http://localhost:8080 on your browser.

Start testing.
