<?php

namespace Api\Controllers;

use App\Models\Users as UsersModel;

class Auth{

	private $model;

	public function __construct(){
		$this->model = new UsersModel();
	}

	public function login(){
		if( isset($_SESSION['auth_user']) || isset($_COOKIE['remember_me']) ){
			http_response_code(200);
			return json_encode(['message' => 'Logged In']);
		}

		$username = isset($_POST['username']) ? $_POST['username'] : null;
		$password = isset($_POST['password']) ? $_POST['password'] : null;
		$remember_me = isset($_POST['remeber_me']) ? true : false;

		if($username && $password){
			
			if($this->model->existsAccount($username, md5($password))){
				$_SESSION['auth_user'] = true;

				if($remember_me){
					//RememberMeToken
					setcookie ("remember_me", true, time()+ ($days * 24 * 60 * 60 * 1000));
				}

				http_response_code(200);
				return json_encode(['message' => 'Logged In']);

			}

			http_response_code(400);
			return json_encode(['message' => 'User or password doesn\'t match our records' ]);

		}
	

		http_response_code(400);
		return json_encode(['message' => 'Not logged In']);
	}

	public function logout(){
		session_destroy();
		return json_encode(['message' => 'Logged out successfully.']);
	}
}