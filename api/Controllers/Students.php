<?php

namespace Api\Controllers;

use App\Models\Students as StudentsModel;

class Students{
	private $model;

	public function __construct(){
		$this->model = new StudentsModel();
	}
	public function index(){
		$per_page = 5;
		$page_num = array_key_exists('page', $_GET) ? $_GET['page'] : 1;

		$results = $this->model->paginate(($page_num-1)*$per_page, ($page_num*$per_page));
		$total = $this->model->getTotal()['total'];

		$pagination = [
			'current_page' => $page_num,
			'total_pages' => ceil($total/5),
			'list' => $results,
		];

		if($pagination['current_page'] != 1){
			$pagination['prev_page'] = $pagination['current_page']-1;
		}

		if($pagination['current_page'] != $pagination['total_pages']){
			$pagination['next_page'] = $pagination['current_page']+1;
		}

		return json_encode($pagination);
	}
}