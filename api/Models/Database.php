<?php

namespace App\Models;

class Database {
	
	public $connection;

	public function __construct(){
		$this->connect();
	}

	private function connect() {
		$this->connection = mysqli_connect(getenv('DB_HOST'), getenv('DB_USERNAME'),getenv('DB_PASSWORD'), getenv('DB_NAME'));
	}


	public function query($query, $first = false){
		$query_results =  $this->connection->query( $query );

		$results = [];

		if ($query_results->num_rows > 0) {
		  while($row = $query_results->fetch_assoc()) {
			if($first){
				return $row;
			}
			array_push($results, $row);
		  }
		} else {
		  return [];
		}
		
		return $results;
	}


}