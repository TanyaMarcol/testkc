<?php

namespace App\Models;

use App\Models\Database;

class Students extends Database{
	
	public function getAll(){
		return $this->query("SELECT * FROM students");
	}

	public function paginate($from_page, $to_page){
		return $this->query("SELECT * FROM students limit $from_page, $to_page");
	}

	public function getTotal(){
		return $this->query('SELECT count(*) as total FROM students', true);
	}
}