$(document).ready(function(){

	function loadStudents ( page = 1 ) {
		$.ajax({
			method: 'GET',
			url: '/students',
			data: 'page='+page
		}).done(response => {
			//llamar a API y loggear
			var pagination = $('#pagination').html('');
			var tbody = $("#list").find('tbody');
			tbody.html('');
			$(response.list).each((row, value) => {
				var full_row = '<tr>'+
									'<td class="p-4 text-right">'+
										'<img src="assets/images/'+ (value.status == 1 ? 'valid' : 'invalid') +'.png" title="Invalid">'+
									'</td>'+
								'<td class="p-4">'+
									'<p>'+ value.username +'</p>'+
									'<p>'+ value.first_name + ' ' +value.first_name+'</p>'+
								'</td>'+
								'<td class="p-4">'+
									'<p>...</p>'+
									'<p>Default group</p>'+
								'</td>'+
							'</tr>';

				tbody.append(full_row);
			});

			if( response.prev_page ){
				pagination.append($('<li>').append($('<a>').html('&laquo; Prev').on('click', function(){
					loadStudents(response.prev_page);
				})));
			}


			$.each(new Array(response.total_pages), (n) => {
				var current = n+1;

				pagination.append($('<li>').append($('<a>').addClass( current == response.current_page ? 'active' : '').text(current).on('click', function(){
						loadStudents(current);
					})));
			});

			if( response.next_page ){
				pagination.append($('<li>').append($('<a>').html('Next &raquo;').on('click', function(){
					loadStudents(response.next_page);
				})));
			}
			
		}).fail(response => {
			console.log(response);
			_alert.text('There was an error sending your request').show();
		});
	}

	
	var checkSession = function(){ $.ajax({
			method: 'POST',
			url: '/auth',
			data: $( this ).serialize()
		}).fail(response => {
			window.location.href = "login.html";
			return false;
		});
		return true;
	};

	if(checkSession()){
		loadStudents();
	}

	$('#logout').on('click', function(){
		//call to api and redirect
		$.ajax({
			method: 'DELETE',
			url: '/auth'
		}).done(response => {
			window.location.href = "login.html";
		});

	});

	return false;

});