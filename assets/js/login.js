
$(document).ready(function(){

	$.ajax({
		method: 'POST',
		url: '/auth'
	}).done(response => {
		window.location.href = "list.html";
		return false;
	});

	$('#login-form').submit(function(e){
		e.preventDefault();


		$.ajax({
			method: 'POST',
			url: '/auth',
			data: $( this ).serialize()
		}).done(response => {
			window.location.href = "list.html";
		}).fail(response => {
			$(this).find('.alert').text(response.responseJSON.message).show();
		});
	});



	$('input').on('focus', function(){
		$('.alert').hide();
	});
});