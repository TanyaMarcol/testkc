<?php

function getAllFiles($folder, &$list = array()) {
	$files = scandir($folder);

	foreach ($files as $file_name) {
		$path = realpath($folder . DIRECTORY_SEPARATOR . $file_name);
		if (!is_dir($path)) {
			if(strpos($path, '.php') !== false){
				$list[] = $path;
			}
		} else if ($file_name != "." && $file_name != "..") {
			getAllFiles($path, $list);
		//	$list[] = $path;
		}
	}

	return $list;
}

$all_files = getAllFiles(__DIR__.'/api');

foreach($all_files as $php_file){
	require_once($php_file);
}
