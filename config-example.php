<?php

$settings = [
  'DB_HOST' => '',
  'DB_PORT' => 3306,
  'DB_USERNAME' => '',
  'DB_PASSWORD' => '',
  'DB_NAME' => ''
];

foreach ($settings as $key => $setting) {
  putenv("$key=$setting");
}