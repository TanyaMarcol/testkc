<?php
session_start();

include('config.php');
include('autoload.php');

use Api\Controllers\Students;
use Api\Controllers\Auth;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$request = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];
$path = $_SERVER['REDIRECT_URL'];
$controller_method = 'index';

switch (substr($path,1)) {
	case 'students':
		$controller = new Students();
	break;
	case 'auth':
		$controller = new Auth();
		if($method == 'POST'){
			$controller_method = 'login';
		}

		if($method == 'DELETE'){
			$controller_method = 'logout';
		}
	break;

	default:
	header('Location: login.html');
	break;
}

try {
	echo $controller->{$controller_method}();
}
catch (Throwable $e) {
	echo $e->getMessage();
}